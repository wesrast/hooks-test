import React, { useReducer } from "react";
import { ProductsContext, ProductsContextProvider } from "./Products.context";

const productReducer = (currentState, { type, payload }) => {
  switch (type) {
    /*case `changeName`:
        return changeProductName(currentState, payload);*/
    case `changePrice`:
      const newProducts = [...currentState.products];
      newProducts[payload.index].price = payload.newPrice;

      return { ...currentState, products: newProducts};
    default:
      return currentState;
  }
};
//example:
//dispatch({type: `changePrice`, payload: 3.50});

const Product = ({data: {name, description, price}, index, changePrice}) => (
  <dl>
    <dt>Product Name:</dt>
    <dd>{name}</dd>
    <dt>Product Desc:</dt>
    <dd>{description}</dd>
    <dt>Product Price:</dt>
    <dd>{price}</dd>
    <dt>Change:</dt>
    <dd><button onClick={() => {changePrice(index, 3.5)}}>change price</button></dd>
  </dl>
);

const ProductList = () => {
  const initialState = React.useContext(ProductsContext);
  const [{products}, dispatch] = useReducer(productReducer, initialState);

  const changePrice = (index, newPrice) => {
    dispatch({type: 'changePrice', payload: {index, newPrice}});
  };

  return products.map((product, i) => (
    <Product key={`dl${i}`} data={product} index={i} changePrice={changePrice} />
  ));
};

const Products = () => {
  return (
    /* the value here has to be an object type */
    <ProductsContextProvider>
      <ProductList />
    </ProductsContextProvider>
  );
};

export default Products;
