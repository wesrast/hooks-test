import React from 'react';

export const ProductsContext = React.createContext();

const products = [
  { name: "one", description: "desc one", price: 1 },
  { name: "two", description: "desc two", price: 2 },
  { name: "three", description: "desc three", price: 3 }
];

export const ProductsContextProvider = ({children}) => (
  <ProductsContext.Provider value={{products}}>
    {children}
  </ProductsContext.Provider>
);

export const ProductsContextConsumer = ProductsContext.Consumer;
