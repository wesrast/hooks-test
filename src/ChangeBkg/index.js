import React, {useState} from "react";

const ChangeBkg = () => {
  const [isItOn, setIsItOn] = useState(true);

  const toggleOn = () => setIsItOn(!isItOn);

  return <>
    <p>Click to change the background</p>
    <p>Is useState there? {typeof useState}</p>
    <button onClick={toggleOn}>Currently is {`${isItOn}`}</button>
  </>;
};

export default ChangeBkg;