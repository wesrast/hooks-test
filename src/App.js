import React, { Component } from 'react';
import ChangeBkg from './ChangeBkg';
import Products from "./Products";

class App extends Component {
  render() {
    return (
      <div className="App">
        <ChangeBkg/>
        <Products />
      </div>
    );
  }
}

export default App;
